/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "AWS IoT Fleet Provisioning", "index.html", [
    [ "Overview", "index.html", [
      [ "Memory Requirements", "index.html#fleet_provisioning_memory_requirements", null ]
    ] ],
    [ "Design", "fleet_provisioning_design.html", null ],
    [ "Configurations", "fleet_provisioning_config.html", [
      [ "FLEET_PROVISIONING_DO_NOT_USE_CUSTOM_CONFIG", "fleet_provisioning_config.html#FLEET_PROVISIONING_DO_NOT_USE_CUSTOM_CONFIG", null ],
      [ "LogError", "fleet_provisioning_config.html#fleet_provisioning_logerror", null ],
      [ "LogWarn", "fleet_provisioning_config.html#fleet_provisioning_logwarn", null ],
      [ "LogInfo", "fleet_provisioning_config.html#fleet_provisioning_loginfo", null ],
      [ "LogDebug", "fleet_provisioning_config.html#fleet_provisioning_logdebug", null ]
    ] ],
    [ "Functions", "fleet_provisioning_functions.html", "fleet_provisioning_functions" ],
    [ "Porting Guide", "fleet_provisioning_porting.html", [
      [ "Configuration Macros", "fleet_provisioning_porting.html#fleet_provisioning_porting_config", null ]
    ] ],
    [ "Data types and Constants", "modules.html", "modules" ],
    [ "Files", "files.html", "files" ]
  ] ]
];

var NAVTREEINDEX =
[
"files.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';