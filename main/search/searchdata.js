var indexSectionsWithContent =
{
  0: "cdefgloptw",
  1: "f",
  2: "cfgpw",
  3: "ft",
  4: "fl",
  5: "ce",
  6: "cdfop"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "enums",
  4: "defines",
  5: "groups",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Enumerations",
  4: "Macros",
  5: "Modules",
  6: "Pages"
};

